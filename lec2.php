<?php
/**
 * Created by PhpStorm.
 * User: spasoyevich_t
 * Date: 12.02.2019
 * Time: 16:47
 */

$n = 100;

echo '<table><tr><th>Число</th><th>Случайное число</th></tr>';
for ($i = 1; $i <= $n; $i++){
    if (($i%2) == 0 ) {
        echo '<tr>';
        echo '<td>' . $i . '</td>';
        echo '<td>' . mt_rand(0, $n) . '</td>';
        echo '</tr>';
    } else {
        echo '<tr style="background: darkgray">';
        echo '<td>' . $i . '</td>';
        echo '<td>' . mt_rand(0, $n) . '</td>';
        echo '</tr>';

    }
}
echo '</table>';

$n = 100;
$k = 5;

echo '<table><tr><th>Число</th><th colspan="'.$k.'">Случайное число</th></tr>';
for ($i = 1; $i <= $n; $i++){
    if (($i%2) == 0 ) {
        echo '<tr>';
        echo '<td>' . $i . '</td>';
        for ($j = 1; $j <= $k; $j++){
            echo '<td>' . mt_rand(0, $n) . '</td>';
        }
        echo '</tr>';
    } else {
        echo '<tr style="background: darkgray">';
        echo '<td>' . $i . '</td>';
        for ($j = 1; $j <= $k; $j++){
            echo '<td>' . mt_rand(0, $n) . '</td>';
        }
        echo '</tr>';

    }
}
echo '</table>';

$n = 10;
$c_text = 150;
$c_back = 255;

$dif = $c_back/$n;


echo '<table><tr><th>Число</th><th>Случайное число</th></tr>';
for ($i = 1; $i <= $n; $i++){
    echo '<tr style="background: rgba('.$c_back.','. $c_back.','. $c_back.'); color: rgb('.$c_text.','. $c_text.','. $c_text.'); ">';
    echo '<td>' . $i . '</td>';
    echo '<td>' . mt_rand(0, $n) . '</td>';
    echo '</tr>';
    $c_text = $c_text-$dif;
    $c_back = $c_back-$dif;
}
echo '</table>';
?>