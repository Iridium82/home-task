<form action="ksr2.php" method="post">
    <p>ЗАДАНИЕ №2</p>
    <p>1. Скрипт, выводящий квадраты и кубы чисел от 2 до n</p>
    <p>Введите число:<input type="text" name="n1" /></p>
    <p><input type="submit" name="submit" value="Выполнить скрипт 1"></p>

    <p>2. Вывести целые числа от 1 до n в таблице из 5 колонок в следующем виде (например, при n=18)</p>
    <p>Максимальное значение n:<input type="text" name="n2" /></p>
    <p>Число столбцов:<input type="text" name="k2" /></p>
    <p>Варианты отображения:</p>
    <p><input type="radio" name="variant" value="Horiz" />Горизонтальный</p>
    <p><input type="radio" name="variant" value="Vert" />Вертикальный </p>
    <p><input type="submit" name="submit" value="Выполнить скрипт 2"></p>

    <p>3. Имеется 2 варианта размещения вклада суммой S в банк.
        В первом случае ежегодно начисляется p процентов.
        Во втором случае ежемесячно начисляется p/12 процентов.
        Cкрипт, который вычисляет и выводит сумму вклада по первому и второму вариантам через 1, 2 , 3, … n лет.</p>
    <p>Введите сумму:<input type="text" name="s" /></p>
    <p>Введите p:<input type="text" name="p" /></p>
    <p>Введите количество лет:<input type="text" name="n3" /></p>
    <p><input type="submit" name="submit" value="Выполнить скрипт 3"></p>
</form>

<?php
if (isset($_POST["submit"])){
    $submit = $_POST["submit"];
    switch ($submit){
        case "Выполнить скрипт 1":
            // Выполнение скрипта № 1.
            if (!empty($_POST["n1"])){
                $n1 = $_POST["n1"];
                echo '<table style="border: 3px solid grey;"><tr style="background: gray;"><th>Число</th><th>Квадрат</th><th>Куб</th></tr>';

                for ($i = 2; $i <= $n1; $i++){
                    $kvd = pow ($i, 2);
                    $kub = pow ($i, 3);
                    echo '<tr>'.'<td>'.$i.'</td>'.'<td>'.$kvd.'</td>'.'<td>'.$kub.'</td>'.'</tr>';
                }

                echo '</table>';
            }
            else {
                echo "Отсутствуют данные для выполнения скрипта. Введите число";
            }
            break;

        case "Выполнить скрипт 2":
            // Выполнение скрипта № 2.
            if (!empty($_POST["n2"]) && !empty($_POST["k2"]) && isset($_POST["variant"])){
                $n2 = $_POST["n2"];
                $k2 = $_POST["k2"];
                $variant = $_POST["variant"];
                switch ($variant){
                    case "Horiz":
                        $count = 1;
                        echo '<table style="border: 3px solid grey;">';
                        /*ВАРИАНТ №1
                         * echo '<tr>';
                         * for ($i = 0; $i < $n2; $i+=$k2){
                         *  if (($i%$k)!= 0){
                         *      echo '<td>'.$i.'</td>';
                         *  } else{
                         *      echo '</tr><tr>';
                                echo '<td>'.$i.'</td>';
                         *  }
                         * }
                         * echo '</tr>';

                         */
                        //ВАРИАНТ №2
                        for ($i = 0; $i < $n2; $i+=$k2){
                            echo '<tr>';
                            for ($j = 0; $j < $k2; $j++) {
                                if ($count <= $n2) {
                                    echo '<td>' . $count . '</td>';
                                    $count++;
                                }
                            }
                            echo '</tr>';

                        }
                        echo '</table>';
                        break;

                    case "Vert":
                        echo '<table style="border: 3px solid grey;">';
                        $count_string = ceil($n2/$k2);
                        for ($i = 0; $i < $count_string; $i++){
                            $count2 = $i+1;
                            echo '<tr>';
                            for ($j = 0; $j < $k2; $j++) {
                                if ($count2 <= $n2) {
                                    echo '<td>' . $count2 . '</td>';
                                    $count2 = $count2 + $count_string;
                                }
                            }
                            echo '</tr>';

                        }

                        echo '</table>';
                        break;

                    default:
                        echo "Ошибка в выборе варианта отображения";
                        break;


                }

            }else{
                echo "Отсутствуют данные для выполнения скрипта";
            }
            break;


        case "Выполнить скрипт 3":
            // Выполнение скрипта № 3.
            if (!empty($_POST["s"]) && !empty($_POST["p"]) && !empty($_POST["n3"])){
                $sum1 = $_POST["s"];
                $sum2 = $_POST["s"];
                $p = $_POST["p"];
                $n3 = $_POST["n3"];
                echo '<table style="border: 3px solid grey;"><tr style="background: gray;"><th>Год</th><th>p процент в год</th><th>p/12 процент в месяц</th></tr>';
                for ($i = 1; $i <= $n3; $i++){
                    echo '<tr>';
                    echo '<td>'.$i.'</td>';
                    $sum1 = $sum1 + $sum1*$p/100;
                    echo '<td>'.$sum1.'</td>';
                    for ($j = 1; $j <= 12; $j++){
                        $sum2 = $sum2 + $sum2*$p/1200;
                    }
                    echo '<td>'.$sum2.'</td>';
                    echo '</tr>';
                }
                echo '</table>';

            }

            break;
        default:
            echo "Ошибка";
            break;

    }
}


?>


