<?php
<?php
class Controller
{
    private $name;
    private $page = 1;
    //следующие 2 переменные для вложенных объектов Model и View
    //чтобы контроллер мог ими управлять
    private $model;
    private $view;
    private $action;

    public function __construct($name, $action)
    {

        $this->name = $name;
        $this->action = $action;

    }

    public function run()
    {
        if (!empty($_GET['page'])) {
            $this->page = intval($_GET['page']);
        }
        //загрузка класса Model
        require 'model/' . $this->name . '.php';
        //создание объекта модели
        $this->model = new Model($this->page, $this->action);
        //выполнение
        $this->model->run();
        //Получение данных от модели
        $data = $this->model->getData();

        //загрузка класса View и передача всех данных для вывода
        if (is_array($data)) { //если не было ошибок
            require 'view/' . $this->name . '.php';
            $this->view = new View($data, $this->page, $this->model->getPageCount());
            $this->view->render(); //вывод данных
        } else { //если возникла ошибка
            require 'view/error.php';
            $this->view = new ViewError($data);
            $this->view->render(); //вывод страницы с сообщением об ошибке
        }
    }
}