<?php
class View
{
    private $data;
    private $page;
    private $pageCount;

    public function __construct($data, $page, $pageCount)
    {
        $this->data = $data;
        $this->page = $page;
        $this->pageCount = $pageCount;
    }

    public function render()
    {
        include 'view/maintemplate.php';
    }
}