<?php
$name = empty($_GET['name'])?'main':$_GET['name'];
$action = empty($_GET['action'])?'show':$_GET['action'];
require 'controller/' . $name . '.php';
$controller = new Controller($name, $action);
$controller->run();