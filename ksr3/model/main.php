<?php
/**
 * Created by PhpStorm.
 * User: Tanya
 * Date: 22.02.2019
 * Time: 20:40
 */

function run($page, &$pageCount)
{
    $data = generateData();
    $itemsPerPage = 18;
    $pageCount = calculatePageCount($data, $itemsPerPage);
    if (checkPageNumber($page, $pageCount)) {
        return readData($data, $page, $itemsPerPage);
    } else {
        return 'Page not found';
    }
}

function generateData()
{
    $text = file_get_contents('text_windows1251.txt');
    $utftext = winToUtf($text);
    $textWithoutTags = strip_tags($utftext);
    funcUpper($utftext);
    $abzacSeparator = "\r\n";
    $abzac = explode($abzacSeparator,$utftext);
    $data = array();
    $countSyms = 0;
    $countWords = 0;
    $countSens = 0;
    foreach ($abzac as $key => $value) {

        $countSym = countSym($value);
        $countWord = countWord($value);
        $countSen = countSen($value);
        //if (mb_substr($value, mb_strlen($value)-1) ==="."){
            //$countSen = $countSen + 1;
            //$countWord = $countWord + 1;
        //}
        $data[] = $value . '</br>';
        $data[] .= "<p>Статистика:</p>";
        $data[] .= "<p>Количество символов: $countSym, количество слов: $countWord, количество предложений: $countSen</p>";

        $countSyms = $countSyms + $countSym;
        $countWords = $countWords + $countWord;
        $countSens = $countSens + $countSen;

    }
    echo "<p>Количество символов: $countSyms, количество слов: $countWords, количество предложений: $countSens</p>";


    return $data;


}

function calculatePageCount($data, $itemsPerPage)
{
    return ceil(count($data)/$itemsPerPage);
}

function checkPageNumber($page, $pageCount)
{
    return $page > 0 && $page <= $pageCount;
}

function readData($data, $page, $itemsPerPage)
{
    $offset = ($page - 1) * $itemsPerPage;
    return array_slice($data, $offset, $itemsPerPage);
}

function winToUtf($stringInWin)
{

    $text = mb_convert_encoding($stringInWin,'UTF-8', 'CP1251');
    return $text;
}

function stringToWords($string, $sep=" ")
{
    $arrayAfter[0] = $string;
    $count = mb_substr_count($string, $sep);
    $countSym = mb_strlen($string);
    $arrayBefore[0] = 0;
        for ($i = 1; $i <= $count; $i++) {
            $pos[$i] = mb_strpos($arrayAfter[$i-1], $sep);
            $arrayAfter[$i] = mb_substr($arrayAfter[$i-1], ($pos[$i]+1));
            $arrayBefore[$i] = mb_substr($arrayAfter[$i-1], 0, ($pos[$i]+1));
        }

            //var_dump($arrayBefore);

    return $arrayBefore;
}


function countWord($value)
{
    $arrayWords = stringToWords($value);
    $separator = "-";
    $count=0;
    foreach ($arrayWords as $key => $value){
        if ($value !== $separator){
            if ($value !== 0) {
                $count++;
            }
        }
    }
    return $count;
}

function countSen($value)
{
    $arraySen = stringToWords($value, $sep = ". ");

    $count=0;

    foreach ($arraySen as $key => $value){
        if ($value !== "0"){
            $count++;
        }
    }
    if (mb_substr($value, mb_strlen($value), 1) === "."){$count++;}



    return $count-1;
}

function countSym($value){
    $arraySym = stringToWords($value);
    //$separator = "-";
    $count=0;
    foreach ($arraySym as $key => $value){
        //if ($value !== $separator){
        $count=$count + mb_strlen($value) + 1;
        //}
    }

    return $count;
}

function funcUpper($value)
{

    $arraySen = stringToWords($value, $sep=". ");
    foreach ($arraySen as $key => $value1){

        $sub = mb_substr($value1,0,1);
        if ($sub===" "){
            $sub = mb_substr($value1,1,1);
            $sub="<b>".$sub."</b>";
            $value1=" ".$sub.(mb_substr($value1,2));
            echo $value1;
        }else {
            $sub = "<b>" . $sub . "</b>";
            $value1 = $sub . (mb_substr($value1, 1));
            echo $value1;
        }

    }
    //return implode($arraySen);
}
