<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Project".
 *
 * @property int $id_project
 * @property int $id_user
 * @property string $pr_name
 * @property string $pr_type
 * @property string $pr_sratus
 * @property string $pr_desc
 * @property string $pr_update
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'pr_name', 'pr_desc'], 'required'],
            [['id_user'], 'integer'],
            [['pr_name', 'pr_type', 'pr_sratus', 'pr_desc'], 'string'],
            [['pr_update'], 'safe'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            //'id_project' => 'Id Project',
            'id_user' => 'Id User',
            'pr_name' => 'Pr Name',
            'pr_type' => 'Pr Type',
            'pr_sratus' => 'Pr Sratus',
            'pr_desc' => 'Pr Desc',
            'pr_update' => 'Pr Update',
        ];
    }
}
